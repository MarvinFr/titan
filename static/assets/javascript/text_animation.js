const TextAnimation = function (element, toRotate, period) {
    this.element = element;
    this.toRotate = toRotate;

    if (period === 'false')
        this.period = 0;
    else
        this.period = parseInt(period, 10) || 2000;

    this.loopNumber = 0;
    this.publishText = '';
    this.tick();
    this.deleting = false;
    this.ready = false;
};

TextAnimation.prototype.tick = function () {
    if (this.ready)
        return;

    const amount = this.loopNumber % this.toRotate.length;
    const text = this.toRotate[amount];

    if (this.deleting)
        this.publishText = text.substring(0, this.publishText.length - 1);
    else
        this.publishText = text.substring(0, this.publishText.length + 1);

    if (this.period === 0)
        this.element.innerHTML = this.publishText;
    else
        this.element.innerHTML = '<span class="wrap">' + this.publishText + "</span>";

    const that = this;
    let delta = 300 - Math.random() * 5;

    if (this.deleting)
        delta /= 12;
    else
        delta /= 3;

    if (!this.deleting && this.publishText === text) {
        if (this.period === 0) {
            this.ready = true;
            return;
        }
        delta = this.period;
        this.deleting = true;
    } else if (this.deleting && this.publishText === '') {
        this.deleting = false;
        this.loopNumber++;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};

window.onload = function () {

    const elements = document.getElementsByClassName('text-animation');

    for (let i = 0; i < elements.length; i++) {
        const toRotate = elements[i].getAttribute('data-rotate');
        const period = elements[i].getAttribute("data-period");

        if (toRotate) {
            new TextAnimation(elements[i], JSON.parse(toRotate), period);
        }
    }

    const css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".wrap { border-right: 0.09em solid #4365ff }";
    document.body.appendChild(css);
};