from search import SymphonySearch


def search(search):
    symphonySearch = SymphonySearch(search)
    array = symphonySearch.search()
    i = 0
    for document in array:
        i = i + 1
        #print("Result: " + str(document))
    return array
