from connector import Connector


def get_status_widget_data():
    newsConnector = Connector('titan', 'newsFeeds')
    sourcesConnector = Connector('titan', 'newsSources')

    document = {
        'newsAmount': newsConnector.get_collection_count(),
        'sourceAmount': sourcesConnector.get_collection_count()
    }
    return document
